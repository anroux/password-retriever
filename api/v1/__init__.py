#!/usr/bin/env python3
from flask import Blueprint
from flask_restful import Api

from .resources.auth import AuthToken
from .resources.password import SelfPassword, OtherPassword
from .resources.team import Team, TeamList
from .resources.token import SelfMachineToken, OtherMachineToken


class CustomApi(Api):
    def handle_error(self, e):
        return {"message": str(e), "details": e.data}, e.code


def create_blueprint(oidc):
    api = CustomApi(catch_all_404s=True)

    api.add_resource(AuthToken(oidc), "/auth/token")

    api.add_resource(TeamList, "/team")
    api.add_resource(Team, "/team/<string:team>")

    api.add_resource(SelfPassword, "/password/<string:hostname>")
    api.add_resource(OtherPassword, "/password/<string:hostname>/<string:team>")

    api.add_resource(SelfMachineToken, "/token/<string:token>")
    api.add_resource(OtherMachineToken, "/token/<string:token>/<string:team>")

    bp = Blueprint("api_v1", __name__)
    api.init_app(bp)

    return bp
