#!/usr/bin/env python3
from flask import abort
from flask_restful import Resource

from utils.authentication import validate_token, current_client
from utils.passgenerator import PasswordEntry

from db import get_team_secret

from sentry_sdk import capture_exception


class MachineToken(Resource):
    def get(self, token, team):
        def json_entry(password, secret):
            return {"value": password.compute(secret)}

        try:
            password = PasswordEntry(token)
            secret = get_team_secret(team)
            return {"request": token, "password": json_entry(password, secret)}
        except Exception as ex:
            capture_exception(ex)
            return {}


class SelfMachineToken(MachineToken):
    @validate_token(["viewer"])
    def get(self, token):
        team = current_client().get("team")
        if team is None:
            abort(403)

        return super().get(token, team)


class OtherMachineToken(MachineToken):
    @validate_token(["admin"])
    def get(self, token, team):
        return super().get(token, team)
