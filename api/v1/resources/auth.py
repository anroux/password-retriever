#!/usr/bin/env python3
from flask_restful import Resource, abort
from flask import request

from utils.authentication import new_token

from sentry_sdk import capture_exception


def AuthToken(oidc):
    class wrapper(Resource):
        @oidc.require_login
        def get(self):
            try:
                client = oidc.user_getfield("name")
                team = oidc.user_getfield("team")
                roles = oidc.user_getfield("roles") or []

                return {"token": new_token(client, team, roles, 30).decode("utf-8")}
            except Exception as ex:
                capture_exception(ex)
                abort(500)

    return wrapper
