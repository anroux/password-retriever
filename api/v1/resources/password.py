#!/usr/bin/env python3
import time
import dns.resolver
import dns.name

from flask import abort
from flask_restful import Resource

from utils.authentication import validate_token, current_client
from utils.passgenerator import PasswordEntry, TokenEntry, normalize_hostname

from db import get_team_secret

from sentry_sdk import capture_exception


class Password(Resource):
    def get(self, hostname, team):
        request = hostname

        def json_entry(password, secret):
            return {
                "token": str(password.token),
                "value": password.compute(secret),
                "expire": password.token.expire(),
            }

        try:
            hostname = normalize_hostname(hostname)
            passwords = [
                PasswordEntry.from_hostname(
                    hostname=hostname,
                    timestamp=time.time() - TokenEntry.DEFAULT_INTERVAL * i,
                )
                for i in range(5)
            ]

            othernames = []
            try:
                parent_domain = dns.name.from_text("esrf.fr")
                answers = dns.resolver.query(hostname, "CNAME")
                othernames = [
                    x.target.relativize(parent_domain).to_text() for x in answers
                ]
            except:
                pass

            secret = get_team_secret(team)
            passwords_json = [json_entry(current, secret) for current in passwords]
            return {
                "request": request,
                "hostname": hostname,
                "othernames": othernames,
                "password": passwords_json[0],
                "previous": passwords_json[1:],
            }
        except Exception as ex:
            capture_exception(ex)
            return {}


class SelfPassword(Password):
    @validate_token(["viewer"])
    def get(self, hostname):
        team = current_client().get("team")
        if team is None:
            abort(403)

        return super().get(hostname, team)


class OtherPassword(Password):
    @validate_token(["admin"])
    def get(self, hostname, team):
        return super().get(hostname, team)
