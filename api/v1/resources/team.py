#!/usr/bin/env python3
import base64

from flask_restful import Resource
from utils.authentication import validate_token

from db import get_team_secret, set_team_secret, delete_team_secret, get_teams


class TeamList(Resource):
    @validate_token(["admin"])
    def get(self):
        teams = get_teams()
        return { "teams": teams }


class Team(Resource):
    @validate_token(["admin"])
    def get(self, team):
        secret = get_team_secret(team)
        return { "secret": base64.b64encode(secret).decode("utf-8") }

    @validate_token(["admin"])
    def post(self, team):
        set_team_secret(team, request.data)
        return { "status": "OK" }, 201

    @validate_token(["admin"])
    def delete(self, team):
        delete_team_secret(team)
        return { "status": "OK" }, 201
