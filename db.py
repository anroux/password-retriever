#!/usr/bin/env python3
import os
import os.path

def get_secret_path(team):
    return os.path.join('teams', os.path.normpath('/' + team.lower()).lstrip('/'))

def get_team_secret(team):
    secret_path = get_secret_path(team)
    with open(secret_path, 'rb') as secret:
        return secret.read()

def set_team_secret(team, secret):
    secret_path = get_secret_path(team)
    with open(secret_path, 'rb') as secret:
        secret.write(secret)

def delete_team_secret(team):
    secret_path = get_secret_path(team)
    os.unlink(secret_path)

def get_teams():
    return [f for f in os.listdir('teams') if os.path.isfile(os.path.join('teams', f))]
