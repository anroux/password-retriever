#!/usr/bin/env python3
import os
import functools
import json
import logging

from flask_restful import abort
from flask import request, g
from sentry_sdk import configure_scope, capture_exception

from cryptography.fernet import Fernet, InvalidToken
from datetime import datetime, timedelta
import dateutil.parser

FERNET_KEY = os.getenv("FERNET_KEY")
if FERNET_KEY is None:
    raise ValueError("The Fernet key must be set (FERNET_KEY envvar)")
else:
    FERNET_KEY = FERNET_KEY.encode("utf-8")


class InvalidTokenException(Exception):
    pass


class DetailedException(Exception):
    @property
    def title(self):
        return super().__str__()

    @property
    def detail(self):
        raise NotImplementedError

    def __str__(self):
        return "{}: {}".format(self.title, self.detail)

    def __repr__(self):
        return self.__str__()


class WrongTokenException(DetailedException):
    def __init__(self, message, what=None, why=None):
        super().__init__(message)
        self.what = what
        self.why = why

    @property
    def detail(self):
        return "{} is incorrect because {}".format(self.what, self.why)


class AccessDeniedException(DetailedException):
    def __init__(self, message, who=None, why=None):
        super().__init__(message)
        self.who = who
        self.why = why

    @property
    def detail(self):
        return "{} forbidden because {}".format(self.who, self.why)


def get_source_ip():
    return (
        request.headers.get("X-Forwarded-For", request.remote_addr)
        .split(",")[0]
        .strip()
    )


def validate_token(required_roles=[]):
    def real_decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            token = request.headers.get("X-Auth-Token", None)
            if token is None:
                abort(401, reason="Missing X-Auth-Token header")

            token_obj = {}
            try:
                f = Fernet(FERNET_KEY)
                json_token = f.decrypt(bytes(token, "utf-8"))
                token_obj = json.loads(json_token.decode("utf-8"))

                logging.debug("Token: {}".format(token_obj))
            except InvalidToken as ex:
                logging.warning("Invalid token provided: {}".format(token))
                capture_exception(ex)
                abort(400, reason="Invalid token provided")

            except Exception as ex:
                capture_exception(ex)
                abort(500)

            try:
                if not "ip_source" in token_obj:
                    raise InvalidTokenException(
                        "Can not check the token source. The token does not contain the source IP."
                    )

                ip_source = token_obj["ip_source"]
                if ip_source != get_source_ip():
                    raise WrongTokenException(
                        "Token used from the wrong source",
                        what="ip_source",
                        why="request = {} / token = {}".format(
                            get_source_ip(), ip_source
                        ),
                    )

                if not "client" in token_obj:
                    raise InvalidTokenException("The client is not set in the token")

                with configure_scope() as scope:
                    scope.user = {"username": token_obj["client"]}

                if not "roles" in token_obj:
                    raise InvalidTokenException(
                        "The client does not have any roles set"
                    )

                if not "expiration" in token_obj:
                    raise InvalidTokenException("Expiration date not set in the token")

                expiration_date = dateutil.parser.parse(token_obj["expiration"])

                if datetime.now() > expiration_date:
                    raise WrongTokenException(
                        "The token is obsolete",
                        what="expiration_date",
                        why="it contains a date in the past",
                    )

                if not all(role in token_obj["roles"] for role in required_roles):
                    raise AccessDeniedException(
                        "Your roles are insufficient",
                        who=token_obj["client"],
                        why="roles [{}] are required while [{}] are provided".format(
                            ", ".join(required_roles), ", ".join(token_obj["roles"])
                        ),
                    )

                g.current_client = token_obj

                return func(*args, **kwargs)

            except InvalidTokenException as ex:
                logging.warning(ex)
                capture_exception(ex)
                abort(400, reason=str(ex))

            except (WrongTokenException, AccessDeniedException) as ex:
                logging.warning(ex)
                capture_exception(ex)
                abort(403, reason=ex.title)

            logging.error("This should not happen!")
            abort(500)

        return wrapper

    return real_decorator


def new_token(client, team, roles, validity=30):
    f = Fernet(FERNET_KEY)
    token_obj = {
        "client": client,
        "team": team,
        "roles": roles,
        "expiration": (datetime.now() + timedelta(minutes=validity)).isoformat(),
        "ip_source": get_source_ip(),
    }
    return f.encrypt(json.dumps(token_obj).encode("utf-8"))


def current_client():
    return getattr(g, "current_client", None)


def require_roles(oidc, asked_roles):
    def real_decorator(func):
        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            user_roles = oidc.user_getfield("roles") or []
            if all(role in user_roles for role in asked_roles):
                return func(*args, **kwargs)

            abort(403)

        return wrapper

    return real_decorator
