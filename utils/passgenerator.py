#!/usr/bin/env python3
import sys
import io
import time
import hashlib
import struct


class CustomPRNG(object):
    def __init__(self, source, seed=0):
        self.source = source
        self.seed = seed

    def next(self, passes=1):
        for i in range(passes):
            a = self.source[self.seed % len(self.source)]
            c = (
                self.source[(self.seed + 1) % len(self.source)]
                * self.source[(self.seed + 2) % len(self.source)]
            ) - 1
            self.seed = (a * self.seed + c) % 58913
        return self.seed


class Humanify(object):
    VOWELS = [
        "a",
        "e",
        "eu",
        "et",
        "er",
        "ei",
        "ai",
        "i",
        "o",
        "au",
        "u",
        "y",
        "ou",
        "oi",
        "an",
        "en",
        "in",
        "ain",
        "un",
        "um",
        "on",
    ]

    CONSONANTS = [
        "b",
        "c",
        "ch",
        "d",
        "f",
        "ph",
        "g",
        "gn",
        "gu",
        "h",
        "j",
        "k",
        "l",
        "m",
        "n",
        "ing",
        "p",
        "q",
        "r",
        "s",
        "t",
        "v",
        "w",
        "x",
        "z",
    ]

    OTHER = ["2", "3", "4", "5", "6", "7", "8", "9"]

    L33T = {"a": ["4"], "i": ["!"], "e": ["3"], "s": ["5", "$"]}

    @classmethod
    def humanify(cls, indigest, length=6):
        prng = CustomPRNG(indigest)

        ret = []
        for i in range(length):
            phonem = None
            if (indigest[0] + i) % 2 == 0:
                phonem = cls.VOWELS[indigest[i] % len(cls.VOWELS)]
            else:
                phonem = cls.CONSONANTS[indigest[i] % len(cls.CONSONANTS)]

            # Transformations
            if prng.next(3) % 11 == 0:
                for src, l33t in cls.L33T.items():
                    phonem = phonem.replace(src, l33t[prng.next(2) % len(l33t)])

            if prng.next(5) % 5 == 0:
                phonem = phonem.upper()

            ret.append(phonem)

            if i < length - 1 and prng.next(7) % 17 == 0:
                ret.append(cls.OTHER[indigest[i] % len(cls.OTHER)])

        return "".join(ret).replace("O", "o").replace("l", "L").replace("I", "i")


class TokenEntry(object):
    DEFAULT_INTERVAL = 60 * 60 * 24 * 14

    def __init__(self, hostname, interval=None, timestamp=None):
        self.hostname = hostname
        self.interval = interval or self.DEFAULT_INTERVAL
        self.timestamp = timestamp or time.time()

    def _timesplice(self):
        return int((self.timestamp or time.time()) / self.interval)

    def hash(self):
        h = hashlib.sha1()
        h.update(self.hostname.encode("utf-8"))
        h.update(struct.pack("<Q", self._timesplice()))
        h.update(struct.pack("<Q", self.interval))

        return h.digest()

    def __str__(self):
        return Humanify.humanify(self.hash(), length=8)

    def expire(self):
        return (
            int((self.timestamp or time.time()) / self.interval + 1) * self.interval - 1
        )


class PasswordEntry(object):
    def __init__(self, token, username="root"):
        self.token = token
        self.username = username

    def hash(self, secret):
        h = hashlib.sha1()
        h.update(secret)
        h.update(str(self.token).encode("utf-8"))
        h.update(self.username.encode("utf-8"))

        return h.digest()

    def compute(self, secret):
        return Humanify.humanify(self.hash(secret))

    @classmethod
    def from_hostname(cls, hostname, username="root", interval=None, timestamp=None):
        token = TokenEntry(hostname, interval=interval, timestamp=timestamp)
        return cls(token, username)


def normalize_hostname(hostname, domain="esrf.fr"):
    if hostname[-1] == ".":
        hostname = hostname[:-1]
    elif "." not in hostname:
        hostname += "." + domain

    return hostname.lower()
