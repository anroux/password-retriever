FROM python:alpine

RUN apk add --no-cache ca-certificates libffi openssl && \
    pip install --upgrade pip && \
    adduser -Sh /usr/src/app -u500 api

WORKDIR /usr/src/app

COPY . /usr/src/app
RUN apk add --no-cache --virtual .build-deps build-base \
        gcc libc-dev rust cargo libffi-dev openssl-dev && \
    pip install --no-cache-dir -r requirements.txt && \
    apk del --no-cache .build-deps

USER api
EXPOSE 8080

CMD [ "uwsgi", "--http-socket", "0.0.0.0:8080", \
               "--master", \
               "--processes", "4", \
               "--wsgi", "wsgi:app" ]
