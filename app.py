#!/usr/bin/env python3
import os
import logging

from flask import Flask
from flask_oidc import OpenIDConnect

import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration

sentry_sdk.init(
    dsn=os.getenv("SENTRY_DSN"),
    integrations=[FlaskIntegration()],
    send_default_pii=True,
)


def create_app():
    app = Flask(__name__)
    app.secret_key = os.getenv("FLASK_SECRET")
    if app.secret_key is None:
        raise ValueError("A Flask secret is needed (FLASK_SECRET envvar)")

    app.config.update(
        {
            "ERROR_404_HELP": False,
            "BUNDLE_ERRORS": True,
            "LOGGING_LEVEL": logging.DEBUG,
            # OIDC
            "OIDC_CLIENT_SECRETS": "client_secrets.json",
            "OIDC_SCOPES": ["openid", "email", "profile"],
            # "OIDC_COOKIE_SECURE": False,
        }
    )

    oidc = OpenIDConnect(app)

    import api.v1
    import views.root

    app.register_blueprint(api.v1.create_blueprint(oidc), url_prefix="/api/v1")
    app.register_blueprint(views.root.create_blueprint(oidc), url_prefix="/")

    return app
