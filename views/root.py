#!/usr/bin/env python3
from flask import Blueprint, render_template, redirect, request, abort
from utils.authentication import require_roles


def create_blueprint(oidc):
    bp = Blueprint("root", __name__)

    @bp.route("/")
    @oidc.require_login
    @require_roles(oidc, ["viewer"])
    def index():
        team = oidc.user_getfield("team")
        roles = oidc.user_getfield("roles") or []
        if team is None:
            abort(403)

        return render_template("index.html", team=team, roles=roles)

    @bp.route("/logout")
    def logout():
        if oidc.user_loggedin:
            oidc.logout()
            return redirect(
                "{logout_endpoint}?redirect_uri={redirect_uri}".format(
                    logout_endpoint=oidc.client_secrets.get("end_session_uri"),
                    redirect_uri=request.url,
                )
            )
        else:
            return render_template("logout.html")

    @bp.errorhandler(500)
    def server_error(error):
        return render_template("internal_error.html"), 500

    return bp
