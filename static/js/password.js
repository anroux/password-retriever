function HostnameViewModel(parent, hostname) {
    var self = this;
    self.parent = parent;
    self.hostname = ko.observable(hostname);

    self.link = ko.pureComputed(function() {
        return '#' + self.hostname();
    }, self);

    self.follow = function() {
        self.parent.request(self.hostname());
    };
}

function PasswordViewModel(json_obj) {
    var self = this;
    self.token = ko.observable(json_obj.token || null);
    self.password = ko.observable(json_obj.value);
    self.expire_ts = ko.observable(json_obj.expire || null);

    self.expiration_full = ko.pureComputed(function() {
        var expiration = new Date(self.expire_ts() * 1000);
        return expiration.toLocaleString('en-GB', { timeZone: 'UTC' });
    }, self);
    self.expiration_date = ko.pureComputed(function() {
        var expiration = new Date(self.expire_ts() * 1000);
        return expiration.toLocaleDateString('en-GB', { timeZone: 'UTC' });
    }, self);
}

function GeneratorViewModel(authApi, initialHostname, callback, team, adminUI = false) {
    var self = this;
    self.authApi = authApi;

    self.passwordURI = '/api/v1/password';
    self.tokenURI = '/api/v1/token';
    self.teamURI = '/api/v1/team';
    self.ajax = authApi.authenticatedAjax;

    self.ready = ko.observable(false);
    self.adminUI = ko.observable(adminUI);

    self.teams = ko.observableArray();
    self.selectedTeam = ko.observable(team);

    self.requestType = ko.observable("hostname");
    self.requestType.subscribe(function() {
        self.request.valueHasMutated();
    });

    self.request = ko.observable();
    self.placeholder = ko.pureComputed(function() {
        return self.requestType() == "token" ? "Token" : "Machine name"
    });
    self.othernames = ko.observableArray();

    self.password = ko.observable();
    self.passwordDisplay = ko.observable("password");
    self.previous = ko.observableArray();

    self.request.subscribe(function() {
        if ((self.request() || '').length < 2) {
            self.password(null);
            self.previous([]);
            self.othernames([]);

            self.authApi.error(false);
            if (callback != null) {
                callback("");
            }
            return;
        }

        request = self.requestType() == "token" ? self.tokenURI : self.passwordURI

        request += '/' + encodeURIComponent(self.request());
        if (self.adminUI() && self.selectedTeam() != null) {
            request += '/' + encodeURIComponent(self.selectedTeam());
        }
        self.ajax(request, 'GET').done(function(data) {
            if (data.request != self.request())
                // Too late...
                return;

            self.password(new PasswordViewModel(data.password));

            if (data.previous) {
                self.previous(data.previous.map(x => new PasswordViewModel(x)));
            } else {
                self.previous([]);
            }

            if (data.previous) {
                self.othernames(data.othernames.map(x => new HostnameViewModel(self, x)));
            } else {
                self.othernames([]);
            }

            self.authApi.error(false);
            if (callback != null) {
                callback(data.hostname || data.request);
            }
        });
    });

    self.togglePassword = function() {
        self.passwordDisplay(self.passwordDisplay() == "password" ? "text" : "password");
    };

    self.selectTeam = function(item) {
        self.selectedTeam(item);
        self.request.valueHasMutated();
        return false;
    };

    self.style = ko.pureComputed(function() {
        if (self.authApi.error()) {
          return 'is-invalid';
        }
        return '';
    }, self);

    self.authApi.dfd.done(function() {
        if (adminUI) {
            self.ajax(self.teamURI, 'GET').done(function(data) {
                self.teams(data.teams);
            });
        }

        self.request(initialHostname);
        self.ready(true);
    });
}
