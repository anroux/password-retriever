function Authentication() {
    var self = this;
    self.dfd = $.Deferred();
    self.error = ko.observable(false);
    self.errorMessage = ko.observable();

    self.authToken = null;
    self.auth = function() {
        var request = {
            url: '/api/v1/auth/token',
            type: 'GET',
            contentType: "application/json",
            accepts: "application/json",
            cache: false,
            dataType: 'json',
            error: function(jqXHR) {
                console.log("Ajax error " + jqXHR.status + ": " + jqXHR.responseJSON['message']);
                self.error(true);
                self.errorMessage(jqXHR.responseJSON['message']);
            }
        };
        $.ajax(request).done(function(data) {
            self.authToken = data.token;
            self.dfd.resolve();
        });

        return self.dfd;
    }

    self.authenticatedAjax = function(uri, method, data) {
        var request = {
            url: uri,
            type: method,
            contentType: "application/json",
            accepts: "application/json",
            cache: false,
            dataType: 'json',
            data: JSON.stringify(data),
            beforeSend: function (xhr) {
                if (self.authToken != null) {
                    xhr.setRequestHeader("X-Auth-Token", self.authToken);
                }
            },
            error: function(jqXHR) {
                reason = jqXHR.responseJSON['details']['reason'] || jqXHR.responseJSON['message'];
                console.log("Authentication error " + jqXHR.status + ": " + reason);

                self.error(true);
                self.errorMessage(reason);
            }
        };
        return $.ajax(request);
    }
}
